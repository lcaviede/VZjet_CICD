#define fatjet_cxx
// The fatjet handler written Bill Murray 5th April 2018

#include "fatjet.h"
#include <iostream>

fatjet::fatjet() : bstatus(-1) {
}

fatjet::~fatjet() {
}

void fatjet::AddTrackJetPointer(trackjet * next) {
  trackJetPointers.push_back(next);  
}


void fatjet::AddMuonPointer(muon * next) {
  muonPointers.push_back(next);
}

trackjet * fatjet::GetTrackVector(uint i) {
  if (  i >= trackJetPointers.size()) {
    std::cout<<"fatjet::GetTrackVector asked for pointer "<<i<<"; size is "<<trackJetPointers.size()<<std::endl;
    return 0;
  }
  return trackJetPointers[i];
}

int  fatjet::GetNumTrackVectors() {
  return trackJetPointers.size();
}

muon * fatjet::GetMuonVector(uint i) {
  if (i >= muonPointers.size()) {
    return 0;
  }
  return muonPointers[i];
}

int fatjet::GetNumMuonVectors() {
  return muonPointers.size();
}

int  fatjet::GetBStatus() {
  // 1: 0 b-tags; 2: 1 85% b-tag; 3: 1 77% b-tag; 4: 1 70% b-tag; 5: 1 60% b-tag; 6: 2 85% b-tags; 7: 2 77% b-tags; 8: 2 70% b-tags; 9: 2 60% b-tags
  if (bstatus < 0 && trackJetPointers.size() > 1) {
    trackjet * leadingtj = 0;
    trackjet * secondtj = 0;
    double highestPt = 0;
    double secondPt = 0;

    for (auto itj = trackJetPointers.begin();itj != trackJetPointers.end();++itj) {
      trackjet * tj = (*itj);
      if (tj->Pt() > highestPt) {
	secondPt = highestPt;
	secondtj = leadingtj;
	leadingtj = tj;
	highestPt = tj->Pt();
      } else if (tj->Pt() > secondPt) {
	secondtj = tj;
	secondPt = tj->Pt();
      } else continue;
    }
    if (leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_60 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_60) bstatus = 9;

    else if (leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_70 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_70) bstatus = 8;

    else if (leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_77 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_77) bstatus = 7;

    else if (leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) bstatus = 6;

    else if ((leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_60 && ! secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) || (! leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_60)) bstatus = 5;

    else if ((leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_70 && ! secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) || (!leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_70)) bstatus = 4;

    else if ((leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_77 && ! secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) || (! leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_77)) bstatus = 3;

    else if ((leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && ! secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) || (! leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85)) bstatus = 2;

    else if (! leadingtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85 && ! secondtj->TrackJetIsBTagged_DL1r_FixedCutBEff_85) bstatus = 1;

   }
    return bstatus;
}

float fatjet::GetXbbDiscriminant(){
  float f_top = 0.25; //recommended
  XbbDiscriminant = -9999;
  //float HbbScoreTop;
  //float HbbScoreQCD;
  //float HbbScoreHiggs;
  XbbDiscriminant = HbbScoreHiggs/((1.0-f_top)*HbbScoreQCD+f_top*HbbScoreTop);
  XbbDiscriminant = log(XbbDiscriminant); 
  return XbbDiscriminant;
}

bool fatjet::GetXbb_50(){
  Xbb_50 = false;
  if(XbbDiscriminant > 3.13) Xbb_50 = true;
  return Xbb_50;
}

bool fatjet::GetXbb_60(){
  Xbb_60 = false;
  if(XbbDiscriminant > 2.55) Xbb_60 = true;
  return Xbb_60;
}

bool fatjet::GetXbb_70(){
  Xbb_70 = false;
  if(XbbDiscriminant > 1.92) Xbb_70 = true;
  return Xbb_70;
}

bool fatjet::GetXbb_80(){
  Xbb_80 = false;
  if(XbbDiscriminant > 1.20) Xbb_80 = true;
  return Xbb_80;
}

bool fatjet::isCand() {
  if(Pt() < 200.) return false;
  if (Eta() > 2. || Eta() < -2.) return false;
  if (M() < 30.) return false;
  //if (GetNumTrackVectors() < 2) return false;
 
  return true;
  }



//////////////////////////////////////////////////////////
// This class has been written by Bill Murray    5th April 2018
//////////////////////////////////////////////////////////

#ifndef fatjet_h
#define fatjet_h

#include "trackjet.h"
#include "muon.h"
#include <TLorentzVector.h>

class fatjet : public TLorentzVector {
 public :
  std::vector<trackjet*> trackJetPointers;
  std::vector<muon*> muonPointers;
  int truthlabel;
  float truthmass;
  int bstatus;
  bool isWTagged_50;
  bool isPassWNtrk_50;
  bool isPassWMassCut_50;
  bool isPassWD2_50;
  bool isWTagged_80;
  bool isPassWNtrk_80;
  bool isPassWMassCut_80;
  bool isPassWD2_80;
  bool isZTagged_50;
  bool isPassZNtrk_50;
  bool isPassZMassCut_50;
  bool isPassZD2_50;
  bool isZTagged_80;
  bool isPassZNtrk_80;
  bool isPassZMassCut_80;
  bool isPassZD2_80;
  bool isMuonCorrection;
  float Ntrk;
  float ECF1;
  float ECF2;
  float ECF3;
  float D2;
  float Tau1_wta;
  float Tau2_wta;
  float Tau3_wta;
  float Tau21;
  float Tau32;
  float FoxWolfram2;
  float FoxWolfram0;
  float Split12;
  float Split23;
  float Qw;
  float PlanarFlow;
  float Angularity;
  float Aplanarity;
  float ZCut12;
  float KtDR;
  float HbbScoreTop;
  float HbbScoreQCD;
  float HbbScoreHiggs;
  float XbbDiscriminant;
  bool Xbb_50;
  bool Xbb_60;
  bool Xbb_70;
  bool Xbb_80;

  fatjet();
  ~fatjet();
  void AddTrackJetPointer(trackjet * next);
  void AddMuonPointer(muon * next);
  trackjet * GetTrackVector(uint i);
  int  GetNumTrackVectors();
  muon * GetMuonVector(uint i);
  int GetNumMuonVectors();
  int  GetBStatus();
  float GetXbbDiscriminant();
  bool GetXbb_50();
  bool GetXbb_60();
  bool GetXbb_70();
  bool GetXbb_80();
  //bool isSelected();
  bool isCand();
  ClassDef(fatjet,0);

};

#endif // #ifdef fatjet_h

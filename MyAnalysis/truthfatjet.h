//////////////////////////////////////////////////////////
// This class has been written by Bill Murray    5th April 2018
//////////////////////////////////////////////////////////

#ifndef truthfatjet_h
#define truthfatjet_h

#include <TLorentzVector.h>

class truthfatjet : public TLorentzVector {
 public :
  int label;
 
  truthfatjet();
  ~truthfatjet();
  ClassDef(truthfatjet,0);

};

#endif // #ifdef truthfatjet_h

echo $PWD
Current=$PWD
#for dsid in 364375 364377 364378 364379 364380 410471 309450 345342 364703 364704 364705 364706 364707 364708 364709 364710 364711 364712
#for dsid in 309450 345342
#for dsid in 346788 346789 #CERN-PROD_PERF-FTAG
#for dsid in 364375 364376 364377 364378 364379 364380 700040 700041 700043 700044 #in IN2P3-CC_PERF-FLAVTAG
for dsid in  700040 700041 700043 700044
do
    rm -rf $dsid
    mkdir ${dsid}
    cd $dsid
    echo $PWD
    file=$( rucio list-datasets-rse IN2P3-CC_PERF-FLAVTAG | grep -e user.yajun:user.yajun.${dsid} | grep -e mc16a | grep -e r9364)
    echo $file
    #rucio list-file-replicas --rse IN2P3-CC_PERF-FLAVTAG --link /eos/:/eos/ ${file}
    rucio list-file-replicas --rse IN2P3-CC_PERF-FLAVTAG ${file} > ${dsid}.txt
    cd ..
done

# The name of the produced library
EXEMC = mc
EXEDAT = data
# The name of the produced library
SHAREDLIB = libMyAnalysis.so
# The name of the produced rootmap file
ROOTMAP = libMyAnalysis.rootmap
# The name of the root flags
ROOT_CFLAGS := $(shell root-config --cflags)
ROOT_GLIBS := $(shell root-config --glibs)
# The name of the python flags
PY_CFLAGS := $(shell python2.7-config --cflags)
PY_LDFLAGS := $(shell python2.7-config --ldflags)

all: ${EXEMC} ${EXEDAT}

# Rule for buildin the exe:
${EXEMC}: ./MyAnalysis/fatjet.cxx ./MyAnalysis/truthfatjet.cxx ./MyAnalysis/trackjet.cxx ./MyAnalysis/muon.cxx ./MyAnalysis/Nominal.cxx Dictionary_mc.cxx
	rm -f ./MyAnalysis/processtype.h
	echo "// Dummy" > ./MyAnalysis/processtype.h
	`root-config --cxx` -o $@ $^ ${ROOT_CFLAGS} -L/cvmfs/sft.cern.ch/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib/ -lGui -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic ${PY_CFLAGS} -Wno-register -L/cvmfs/sft.cern.ch/lcg/releases/LCG_96/Python/2.7.16/x86_64-centos7-gcc8-opt/lib ${PY_LDFLAGS}

${EXEDAT}: ./MyAnalysis/fatjet.cxx ./MyAnalysis/truthfatjet.cxx ./MyAnalysis/trackjet.cxx ./MyAnalysis/muon.cxx ./MyAnalysis/Nominal.cxx Dictionary_dat.cxx
	rm -f ./MyAnalysis/processtype.h
	echo "#define processdata" > ./MyAnalysis/processtype.h
	`root-config --cxx` -o $@ $^ ${ROOT_CFLAGS} -L/cvmfs/sft.cern.ch/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc8-opt/lib -lGui -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic ${PY_CFLAGS} -Wno-register -L/cvmfs/sft.cern.ch/lcg/releases/LCG_96/Python/2.7.16/x86_64-centos7-gcc8-opt/lib ${PY_LDFLAGS}

# Rule for making a single dictionary for the sources:
Dictionary_dat.cxx: ./MyAnalysis/fatjet.h ./MyAnalysis/truthfatjet.h ./MyAnalysis/trackjet.h ./MyAnalysis/muon.h ./MyAnalysis/Nominal.h ./MyAnalysis/LinkDef.h
	rm -f ./MyAnalysis/processtype.h
	echo "#define processdata" > ./MyAnalysis/processtype.h
	rootcint -f $@ -s ${SHAREDLIB} -rml ${SHAREDLIB} -rmf ${ROOTMAP} $^

Dictionary_mc.cxx: ./MyAnalysis/fatjet.h ./MyAnalysis/truthfatjet.h ./MyAnalysis/trackjet.h ./MyAnalysis/muon.h ./MyAnalysis/Nominal.h ./MyAnalysis/LinkDef.h
	rm -f ./MyAnalysis/processtype.h
	echo "// Dummy" > ./MyAnalysis/processtype.h
	rootcint -f $@ -s ${SHAREDLIB} -rml ${SHAREDLIB} -rmf ${ROOTMAP} $^

# Cleaning rule(s):
clean:
	rm -f *.o	
	rm -f Dictionary*
	rm -f libMyAnalysis*
	rm -f mc
	rm -f data
	rm -f ./MyAnalysis/processtype.h
	rm -f *~
	rm -rf ./MyAnalysis/*~

#General build rules:
.SUFFIXES: .cxx .o

# Rule for making an object file out of a source file:
.cxx.o:
	`root-config --cxx` -c -g -02 -fPIC -o $@ `root-config --cflags`$<


